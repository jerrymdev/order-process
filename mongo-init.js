db = db.getSiblingDB('order_db')


db.createUser({
    user: 'order_user',
    pwd: 'order_pass',
    roles: [
        {
            role: 'dbOwner',
            db: 'order_db',
        },
    ],
});