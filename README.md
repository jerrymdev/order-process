## Pre requisitos:
- linux
- maven
- docker
- docker compose


## Para executar o projeto

``` bash
docker compose build --no-cache
docker compose up
```

## Para acessar o client graphql
[http://localhost:8080/graphiql](http://localhost:8080/graphiql)

- Usuario: user
- Senha: password

### queries de exemplo

``` bash
mutation {
  publishPlacedOrderMessage(
    order: {
      id: "88812", 
      customerName: "Jerry Souza",
      items: [
        {productId: "444", quantity: 2, price: 150.5},
        {productId: "444", quantity: 1, price: 150.3}
      ], 
      total: 300.8
    }
  ) {
    id
  }
}
```

``` bash
query {
  placedOrders(
	customerName: "Jerry Souza"
    minTotal: 100,
    maxTotal: 30000,
    minCreatedDate: "2023-01-01T00:00:00Z",
    maxCreatedDate: "2023-12-03T23:00:00Z"
  ) {
    id
    customerName,
    total
  }
}
```

``` bash
query {
  orderStatistics{
        totalSales,
        totalOrders,
        mostSoldProductId
  }
}

```
