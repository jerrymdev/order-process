package com.order.domain.usecase;

import com.order.domain.model.OrderDomain;
import com.order.core.database.gateway.OrderDatabaseGateway;
import com.order.domain.port.SaveOrder;
import org.springframework.stereotype.Service;

@Service
public class SaveOrderUseCase implements SaveOrder {
    private final OrderDatabaseGateway orderDatabaseGateway;

    public SaveOrderUseCase(OrderDatabaseGateway orderDatabaseGateway) {
        this.orderDatabaseGateway = orderDatabaseGateway;
    }

    @Override
    public void execute(OrderDomain order) {
        orderDatabaseGateway.save(order);
    }
}
