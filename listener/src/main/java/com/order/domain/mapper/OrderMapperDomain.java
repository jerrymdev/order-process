package com.order.domain.mapper;


import com.order.entity.OrderEntity;
import com.order.domain.model.OrderDomain;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;

@Mapper(componentModel = "spring", imports = {LocalDateTime.class})
public interface OrderMapperDomain {

    @Mapping(target = "createdDate", defaultExpression ="java(LocalDateTime.now())")
    OrderEntity toEntity(OrderDomain domain);


    OrderDomain toDomain(OrderEntity entity);
}
