package com.order.domain.port;

import com.order.domain.model.OrderDomain;

public interface SaveOrder {
    void execute(OrderDomain order);
}
