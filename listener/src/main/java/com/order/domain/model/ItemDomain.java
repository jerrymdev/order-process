package com.order.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public record ItemDomain(
        @JsonProperty("productId") String productId,
        @JsonProperty("quantity") int quantity,
        @JsonProperty("price") double price
) implements Serializable {
}

