package com.order.core.database.gateway;

import com.order.domain.mapper.OrderMapperDomain;
import com.order.domain.model.OrderDomain;
import com.order.repository.OrderRepository;
import org.springframework.stereotype.Component;

@Component
public class OrderDatabaseGateway {
    private final OrderRepository orderRepository;
    private final OrderMapperDomain mapper;

    public OrderDatabaseGateway(OrderRepository orderRepository, OrderMapperDomain mapper) {
        this.orderRepository = orderRepository;
        this.mapper = mapper;
    }

    public void save(OrderDomain orderDomain) {
        if (!orderRepository.existsById(orderDomain.id())) {
            orderRepository.save(mapper.toEntity(orderDomain));
        }
    }
}
