package com.order.core.message.gateway;

import com.order.domain.model.OrderDomain;
import com.order.domain.usecase.SaveOrderUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class QueueConsumerGateway {
    private final SaveOrderUseCase saveOrderUseCase;

    private final Logger log = LoggerFactory.getLogger(QueueConsumerGateway.class);

    public QueueConsumerGateway(SaveOrderUseCase saveOrderUseCase) {
        this.saveOrderUseCase = saveOrderUseCase;
    }

    @RabbitListener(queues = {"${rabbitmq.queue.json.name}"})
    public void receive(@Payload OrderDomain message) {
        log.info("mensagem recebida order={}", message);
        saveOrderUseCase.execute(message);
    }

}