#!/bin/bash

set -e

# Inicie o RabbitMQ server
rabbitmq-server &
sleep 10  # Aguarde alguns segundos para o servidor iniciar corretamente

# Comandos para criar a fila
rabbitmqadmin declare queue name=order_queue durable=true

# Mantenha o contêiner em execução
tail -f /dev/null
