package com.order.core.database.gateway;

import com.order.criteria.CreatedDateRangeCriteria;
import com.order.criteria.CustomerNameCriteria;
import com.order.criteria.OrderCriteria;
import com.order.criteria.TotalRangeCriteria;
import com.order.domain.model.OrderStatsDomain;
import com.order.domain.model.RequestFilter;
import com.order.domain.usecase.FetchOrdersStatsUseCase;
import com.order.entity.OrderEntity;
import com.order.gateway.OrderEntitySearchService;
import com.order.repository.OrderRepository;
import com.order.domain.mapper.OrderMapperDomain;
import com.order.domain.model.OrderDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderDatabaseGateway {
    private final OrderRepository orderRepository;
    private final OrderEntitySearchService searchService;
    private final OrderMapperDomain mapper;

    private final Logger log = LoggerFactory.getLogger(OrderDatabaseGateway.class);

    public OrderDatabaseGateway(OrderRepository orderRepository, OrderEntitySearchService searchService, OrderMapperDomain mapper) {
        this.orderRepository = orderRepository;
        this.searchService = searchService;
        this.mapper = mapper;
    }

    public List<OrderDomain> findOrdersByCriteria(RequestFilter filter) {
        try {
            List<OrderCriteria> criteriaList = new ArrayList<>();

            if (filter.customerName() != null) {
                criteriaList.add(new CustomerNameCriteria(filter.customerName()));
            }

            if (filter.minTotal() != null && filter.maxTotal() != null) {
                criteriaList.add(new TotalRangeCriteria(filter.minTotal(), filter.maxTotal()));
            }

            if (filter.minCreatedDate() != null && filter.maxCreatedDate() != null) {
                criteriaList.add(new CreatedDateRangeCriteria(filter.minCreatedDate(), filter.maxCreatedDate()));
            }

            List<OrderEntity> orders = searchService.findOrdersByCriteria(criteriaList);

            return orders.stream().map(mapper::toDomain).collect(Collectors.toList());

        } catch (Exception e) {
            log.error("erro ao buscar orders");
            return List.of();
        }
    }

    public OrderStatsDomain getOrdersStats() {
        log.error("erro ao buscar orders");
        return mapper.toDomain(orderRepository.findOrderStats());
    }
}
