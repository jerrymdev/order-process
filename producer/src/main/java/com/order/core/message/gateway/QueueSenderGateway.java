package com.order.core.message.gateway;

import com.order.domain.model.OrderDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class QueueSenderGateway {
    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.routing.json.key}")
    private String routingJsonKey;

    private final RabbitTemplate rabbitTemplate;

    private final Logger log = LoggerFactory.getLogger(QueueSenderGateway.class);

    public QueueSenderGateway(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void send(OrderDomain message) {
        log.info("enviando mensagem order={}", message);
        rabbitTemplate.convertAndSend(exchange, routingJsonKey, message);
    }
}