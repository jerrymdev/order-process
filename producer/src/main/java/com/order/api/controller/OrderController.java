package com.order.api.controller;

import com.order.api.mapper.DateUtil;
import com.order.domain.model.OrderStatsDomain;
import com.order.domain.model.RequestFilter;
import com.order.domain.port.FetchOrders;
import com.order.domain.port.FetchOrdersStats;
import com.order.domain.port.SendOrder;
import com.order.api.dto.OrderDTO;
import com.order.api.mapper.OrderMapperApi;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class OrderController {
    private final OrderMapperApi mapper;
    private final SendOrder sendOrderService;
    private final FetchOrders fetchOrdersService;

    private final FetchOrdersStats fetchOrdersStats;

    public OrderController(OrderMapperApi mapper, SendOrder sendOrderService, FetchOrders fetchOrdersService, FetchOrdersStats fetchOrdersStats) {
        this.mapper = mapper;
        this.sendOrderService = sendOrderService;
        this.fetchOrdersService = fetchOrdersService;
        this.fetchOrdersStats = fetchOrdersStats;
    }

    @MutationMapping
    public void publishPlacedOrderMessage(@Argument OrderDTO order) {
        sendOrderService.execute(mapper.toDomain(order));
    }

    @QueryMapping
    public List<OrderDTO> placedOrders(@Argument String customerName, @Argument Double minTotal, @Argument Double maxTotal, @Argument String minCreatedDate, @Argument String maxCreatedDate) {
        RequestFilter filterCriteria = new RequestFilter(customerName, minTotal, maxTotal, DateUtil.toDate(minCreatedDate), DateUtil.toDate(maxCreatedDate));
        return fetchOrdersService.execute(filterCriteria).stream().map(mapper::toDTO).collect(Collectors.toList());
    }

    @QueryMapping
    public OrderStatsDomain orderStatistics() {
        return fetchOrdersStats.execute();
    }
}
