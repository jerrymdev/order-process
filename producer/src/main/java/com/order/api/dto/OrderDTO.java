package com.order.api.dto;

import java.time.LocalDateTime;
import java.util.List;

public record OrderDTO(
        String id,
        String customerName,
        List<ItemDTO> items,
        Double total,
        LocalDateTime createdDate
) {
}
