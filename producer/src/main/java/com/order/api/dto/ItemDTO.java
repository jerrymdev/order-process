package com.order.api.dto;

public record ItemDTO(
        String productId,
        int quantity,
        double price
) {
}
