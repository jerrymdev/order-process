package com.order.api.mapper;

import com.order.api.controller.OrderController;
import com.order.api.dto.OrderDTO;
import com.order.domain.model.OrderDomain;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = OrderController.class, componentModel = "spring")
public interface OrderMapperApi {
    OrderDomain toDomain(OrderDTO dto);
    OrderDTO toDTO(OrderDomain domain);
}
