package com.order.domain.mapper;


import com.order.domain.model.OrderStatsDomain;
import com.order.entity.OrderEntity;
import com.order.domain.model.OrderDomain;
import com.order.entity.OrderStatsEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OrderMapperDomain {

    @Mapping(target = "id", ignore = true)
    OrderEntity toEntity(OrderDomain domain);


    OrderDomain toDomain(OrderEntity entity);

    OrderStatsDomain toDomain(OrderStatsEntity entity);
}
