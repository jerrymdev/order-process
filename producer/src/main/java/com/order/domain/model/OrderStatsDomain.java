package com.order.domain.model;

import java.io.Serializable;

public record OrderStatsDomain(
        Double totalSales,
        Long totalOrders,
        String mostSoldProductId
) implements Serializable {
}
