package com.order.domain.model;

import java.io.Serializable;

public record ItemDomain(
        String productId,
        int quantity,
        double price
) implements Serializable {
}

