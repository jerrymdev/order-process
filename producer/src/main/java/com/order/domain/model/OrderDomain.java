package com.order.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public record OrderDomain(
        @JsonProperty("id") String id,
        @JsonProperty("customerName") String customerName,
        @JsonProperty("items") List<ItemDomain> items,
        @JsonProperty("total") double total,
        LocalDateTime createdDate
) implements Serializable {
}