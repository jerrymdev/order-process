package com.order.domain.model;

import java.time.LocalDateTime;

public record RequestFilter(String customerName,
                            Double minTotal,
                            Double maxTotal,
                            LocalDateTime minCreatedDate,
                            LocalDateTime maxCreatedDate) {
}
