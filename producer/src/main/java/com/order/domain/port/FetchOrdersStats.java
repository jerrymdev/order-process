package com.order.domain.port;

import com.order.domain.model.OrderStatsDomain;

public interface FetchOrdersStats {
    OrderStatsDomain execute();
}
