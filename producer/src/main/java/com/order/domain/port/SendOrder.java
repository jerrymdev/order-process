package com.order.domain.port;

import com.order.domain.model.OrderDomain;

public interface SendOrder {
    void execute(OrderDomain order);
}
