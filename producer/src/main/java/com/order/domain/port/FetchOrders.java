package com.order.domain.port;

import com.order.domain.model.OrderDomain;
import com.order.domain.model.RequestFilter;

import java.util.List;

public interface FetchOrders {
    List<OrderDomain> execute(RequestFilter requestFilter);
}
