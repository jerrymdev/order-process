package com.order.domain.usecase;

import com.order.core.message.gateway.QueueSenderGateway;
import com.order.domain.port.SendOrder;
import com.order.domain.model.OrderDomain;
import org.springframework.stereotype.Service;

@Service
public class SendOrderUseCase implements SendOrder {

    private final QueueSenderGateway queueSenderGateway;

    public SendOrderUseCase(QueueSenderGateway queueSenderGateway) {
        this.queueSenderGateway = queueSenderGateway;
    }

    @Override
    public void execute(OrderDomain order) {
        queueSenderGateway.send(order);
    }
}
