package com.order.domain.usecase;

import com.order.core.database.gateway.OrderDatabaseGateway;
import com.order.domain.model.OrderDomain;
import com.order.domain.model.RequestFilter;
import com.order.domain.port.FetchOrders;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FetchOrdersUseCase implements FetchOrders {

    private final OrderDatabaseGateway orderDatabaseGateway;

    public FetchOrdersUseCase(OrderDatabaseGateway orderDatabaseGateway) {
        this.orderDatabaseGateway = orderDatabaseGateway;
    }

    @Override
    public List<OrderDomain> execute(RequestFilter requestFilter) {
        return orderDatabaseGateway.findOrdersByCriteria(requestFilter);
    }
}
