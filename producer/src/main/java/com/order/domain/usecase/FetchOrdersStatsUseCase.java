package com.order.domain.usecase;

import com.order.core.database.gateway.OrderDatabaseGateway;
import com.order.domain.model.OrderStatsDomain;
import com.order.domain.port.FetchOrdersStats;
import org.springframework.stereotype.Service;

@Service
public class FetchOrdersStatsUseCase implements FetchOrdersStats {
    private final OrderDatabaseGateway orderDatabaseGateway;

    public FetchOrdersStatsUseCase(OrderDatabaseGateway orderDatabaseGateway) {
        this.orderDatabaseGateway = orderDatabaseGateway;
    }

    @Override
    public OrderStatsDomain execute() {
        return orderDatabaseGateway.getOrdersStats();
    }
}
