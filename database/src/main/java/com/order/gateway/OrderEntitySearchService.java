package com.order.gateway;

import com.order.criteria.OrderCriteria;
import com.order.entity.OrderEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrderEntitySearchService {

    private final MongoTemplate mongoTemplate;

    public OrderEntitySearchService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    public List<OrderEntity> findOrdersByCriteria(List<OrderCriteria> criteriaList) {
        Criteria criteria = new Criteria();
        if (!CollectionUtils.isEmpty(criteriaList)) {
            List<Criteria> andCriteriaList = new ArrayList<>();
            for (OrderCriteria orderCriteria : criteriaList) {
                andCriteriaList.add(orderCriteria.buildCriteria());
            }
            criteria.andOperator(andCriteriaList.toArray(new Criteria[0]));
        }

        Query query = new Query(criteria);
        return mongoTemplate.find(query, OrderEntity.class);
    }

}
