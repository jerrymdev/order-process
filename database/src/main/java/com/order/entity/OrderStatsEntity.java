package com.order.entity;

import java.io.Serializable;

public record OrderStatsEntity(
        Double totalSales,
        Long totalOrders,
        String mostSoldProductId
) implements Serializable {
}