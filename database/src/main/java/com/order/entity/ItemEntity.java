package com.order.entity;

import org.springframework.data.annotation.Id;

import java.io.Serializable;

public record ItemEntity(
        @Id String productId,
        Integer quantity,
        Double price
) implements Serializable {
}

