package com.order.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

@Document(collection = "orders")
public record OrderEntity(
        @Id String id,
        @Indexed
        String customerName,
        List<ItemEntity> items,
        @Indexed
        double total,
        @Indexed
        @CreatedDate
        LocalDateTime createdDate
) implements Serializable {
}