package com.order.repository;

import com.order.entity.OrderEntity;
import com.order.entity.OrderStatsEntity;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface OrderRepository extends MongoRepository<OrderEntity, String> {

    @Aggregation(pipeline = {
            "{$group: {_id: null, totalSales: {$sum: \"$total\"}, totalOrders: {$sum: 1}, items: {$push: \"$items\"}}}",
            "{$unwind: \"$items\"}",
            "{$unwind: \"$items\"}",
            "{$group: {_id: \"$items.productId\", totalQuantity: {$sum: \"$items.quantity\"}, item: {$first: \"$items\"}, totalSales: {$first: \"$totalSales\"}, totalOrders: {$first: \"$totalOrders\"}}}",
            "{$sort: {totalQuantity: -1}}",
            "{$limit: 1}",
            "{$project: {_id: 0, totalSales: 1, totalOrders: 1, mostSoldProductId: \"$_id\"}}"
    })
    OrderStatsEntity findOrderStats();

}
