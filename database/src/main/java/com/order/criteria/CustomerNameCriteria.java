package com.order.criteria;

import org.springframework.data.mongodb.core.query.Criteria;

public class CustomerNameCriteria implements OrderCriteria {
    private final String customerName;

    public CustomerNameCriteria(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public Criteria buildCriteria() {
        return Criteria.where("customerName").is(customerName);
    }
}
