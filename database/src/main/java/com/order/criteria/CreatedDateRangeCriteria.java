package com.order.criteria;
import org.springframework.data.mongodb.core.query.Criteria;
import java.time.LocalDateTime;

public class CreatedDateRangeCriteria implements OrderCriteria {
    private final LocalDateTime minCreatedDate;
    private final LocalDateTime maxCreatedDate;

    public CreatedDateRangeCriteria(LocalDateTime minCreatedDate, LocalDateTime maxCreatedDate) {
        this.minCreatedDate = minCreatedDate;
        this.maxCreatedDate = maxCreatedDate;
    }

    @Override
    public Criteria buildCriteria() {
        Criteria criteria = new Criteria();
        if (minCreatedDate != null || maxCreatedDate != null) {
            criteria = Criteria.where("createdDate");
            if (minCreatedDate != null) {
                criteria = criteria.gte(minCreatedDate);
            }
            if (maxCreatedDate != null) {
                criteria = criteria.lte(maxCreatedDate);
            }
        }
        return criteria;
    }
}

