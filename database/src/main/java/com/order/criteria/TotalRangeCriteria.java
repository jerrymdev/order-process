package com.order.criteria;

import org.springframework.data.mongodb.core.query.Criteria;

public class TotalRangeCriteria implements OrderCriteria {
    private final double minTotal;
    private final double maxTotal;

    public TotalRangeCriteria(double minTotal, double maxTotal) {
        this.minTotal = minTotal;
        this.maxTotal = maxTotal;
    }

    @Override
    public Criteria buildCriteria() {
        return Criteria.where("total").gte(minTotal).lte(maxTotal);
    }
}
