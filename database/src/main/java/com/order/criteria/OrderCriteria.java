package com.order.criteria;

import org.springframework.data.mongodb.core.query.Criteria;

public interface OrderCriteria {
    Criteria buildCriteria();
}